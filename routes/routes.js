const Schema = require('../schemas/schemas');
const Middlewares = require('../middlewares/middlewares');
const Controller = require('../controllers/userController');

module.exports = [

    {
        method: 'get',
        route: '/',
        controller: Controller.index,
    },
    {
        method: 'get',
        route: '/redirectGoogle',
        // schema_validation: Schema.googleRedirect,
        controller: Controller.googleRedirect,
    },

];
