const jwt = require('jsonwebtoken');
const UserModel = require('../models/userModel');


function getJwtExpirationTime() {
    return Math.floor(Date.now() / 1000) + 604800;
}

exports.getJwtExpirationTime = getJwtExpirationTime;

exports.extractAccessToken = (req) => {
    let token = null;
    if (req.headers.authorization) {
        token = req.headers.authorization;
    }
    return token;
};

exports.setJwt = (id) => {
    return jwt.sign({_id: id}, 'piyush');
};
