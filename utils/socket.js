const Filter = require('bad-words');
const socketIO = require('socket.io');
module.exports = (server) => {
    const io = socketIO(server);
    var generateMessage = (message) => {
        return {
            message: message
        }
    };

    io.on('connection', (socket) => {
        const message = 'Hi welcome to app';
        socket.emit('welcome', generateMessage(message), user => {

        });

        socket.broadcast.emit('message', generateMessage('A new user have joined'));

        socket.on('sendMessage', (message, callback) => {
            const filter = new Filter();
            if (filter.isProfane(message)) {
                callback('Profanity is not allowed')
            }
            io.emit('message', generateMessage(message));
            callback('Delivered and acknowledged')
        });

        socket.on('broadcastUrl', (url, callback) => {
            socket.broadcast.emit('getUrl', generateMessage(url));
            callback();
        });

        socket.on('disconnect', () => {
            socket.broadcast.emit('message', generateMessage('A user have disconnected'))
        })

    });
};
