const {google} = require('googleapis');
const axios = require('axios').default;

const googleConfig = {
    clientId: '103650237967-ksv7p6flbu3bvgaauq3kjnip7nnpqug3.apps.googleusercontent.com', // e.g. asdfghjkljhgfdsghjk.apps.googleusercontent.com
    clientSecret: 'zI4W7cgBrRBWxhIwvE_J83dL', // e.g. _ASDFA%DFASDFASDFASD#FAD-
    redirect: 'http://broadcast.unfoldingskies.com:8088/redirectGoogle', // this must match your google api settings
};

const defaultScope = [
    'https://www.googleapis.com/auth/plus.me',
    'https://www.googleapis.com/auth/userinfo.email'
];

var getDatafromToken = async (code) => {
    const token = await axios({
        url: `https://oauth2.googleapis.com/token`,
        method: 'post',
        data: {
            client_id: googleConfig.clientId,
            client_secret: googleConfig.clientSecret,
            redirect_uri: googleConfig.redirect,
            grant_type: 'authorization_code',
            code,
        },
    });

    const {data} = await axios({
        url: 'https://www.googleapis.com/oauth2/v2/userinfo',
        method: 'get',
        headers: {
            Authorization: `Bearer ${token.access_token}`,
        },
    });
    return data;
};

exports.userData = getDatafromToken;

var createConnection = () => {
    return new google.auth.OAuth2(
        googleConfig.clientId,
        googleConfig.clientSecret,
        googleConfig.redirect
    );
};

exports.getConnectionUrl = async () => {
    return createConnection().generateAuthUrl({
        access_type: 'offline',
        prompt: 'consent',
        scope: defaultScope
    });
};
