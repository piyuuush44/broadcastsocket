const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const Schema = mongoose.Schema;

let userModel = new Schema({
    email: {type: String, required: true},
}, {timestamps: true});

userModel.methods.generateAuth = function () {
    return jwt.sign({_id: this._id}, 'piyush');
};
userModel = mongoose.model('users', userModel);

module.exports = userModel;
