const { Joi } = require('celebrate');
module.exports = {
    googleRedirect: {
        query: Joi.object().keys({
            code: Joi.string().trim(),
        }),
    },
};
