const asyncHandler = require('express-async-handler');
const jwt = require('jsonwebtoken');

const UserModel = require('../models/userModel');

const authUtils = require('../utils/auth');

exports.verifyJwt = asyncHandler(async (req, res, next) => {

    // Extract the token
    const accessToken = authUtils.extractAccessToken(req) || '';
    if (!accessToken) {
        return res.status(400).json('no token found');
    }

    // Verify the JWT
    try {
        const jwtPayload = jwt.verify(accessToken, 'piyush');

        // Fetch the agent and set the agent object on the request
        const id = parseInt(jwtPayload.data.id, 10);
        req.user = await UserModel.findById(id);
        next();
    } catch (e) {
        return res.status(400).json('token invalid');
    }
});
