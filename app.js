const {isCelebrate} = require('celebrate');
const logger = require('./config/logger');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const routes = require('./routes');

const express = require('express');
const app = express();

//templating engine
app.set('view engine', 'ejs');

//for serving static path
app.use(express.static(path.join(__dirname, 'public')));

//parsing the body
app.use(bodyParser.json({limit: '10MB'}));
app.use(bodyParser.urlencoded({
    extended: false
}));

//for cors functionality
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,PATCH');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization,x-auth-token');
    res.setHeader('Access-Control-Expose-Headers', 'x-auth-token');
    next();
});

// winston logger
logger.stream = {
    write: function (message, encoding) {
        logger.info(message);
    }
};

// for db connection
const db = 'youtube';
var dbConnect = async () => {
    const mongo = await mongoose.connect('mongodb+srv://piyush:piyush@piyush-jk7ld.mongodb.net/' + db + '?retryWrites=true&w=majority', {
        useNewUrlParser: true, useUnifiedTopology: true
    });
};
dbConnect();

//function to serve all our routes
routes(app);

//to generate error response dynamically
var getErrorResponse = (httpStatus, message) => {
    return {
        error: {
            httpStatus: httpStatus,
            message: message
        }
    }
};

// app.use((err, req, res, next) => {
//     logger.error(err);
//
//     if (isCelebrate(err)) {
//         if (err.joi.details && err.joi.details.length > 0) {
//             res.status(400).send(getErrorResponse(400, err.joi.details[0].message));
//         } else {
//             res.status(400).send(getErrorResponse(400, 'Input validation error'));
//         }
//     } else if (err.name === 'UnauthorizedError') {
//         res.status(401).send(getErrorResponse(401, 'Unauthorized. Missing or invalid token'));
//     } else {
//         // If it is an uncaught exception, pass it back as an Internal Server Error
//         res.status(500).send(getErrorResponse(500, 'Looks like something went wrong. Please wait and try again in a few minutes.'));
//     }
// });
module.exports = app;
