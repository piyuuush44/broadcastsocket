const http = require('http');
const app = require('./app');
const server = http.createServer(app);
const socket = require('./utils/socket')(server);
const port = process.env.PORT || 8088;
// const port = process.env.PORT || 3000;
server.listen(port, () => console.log(`connected to port ${port}`));
