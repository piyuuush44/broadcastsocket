const helper = require('../utils/google');
const auth = require('../utils/auth');
const user = require('../models/userModel');

exports.index = async (req, res, next) => {
    return res.render('index', {url: await helper.getConnectionUrl()});
};

exports.googleRedirect = async (req, res, next) => {
    const code = req.query.code;

    if (code) {
        const data = await helper.userData(code);
        let user = await user.findOne({email: data.email});

        // user found
        if (user) {
            return res.render('home', {token: auth.setJwt(user._id), user: user});
        }
        // user not found
        user.email = data.email;
        const result = await user.save();
        if (result) return res.render('home', {token: auth.setJwt(user._id), user: user});
    }
};
